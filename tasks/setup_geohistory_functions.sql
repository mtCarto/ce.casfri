---
- name: Run the GeoHistory SQL files
  shell: 
    cmd: |
      {{ casfri_psql_bin_dir }}/psql {{ pgsql_trans_fw_db }} <<EOF
      set search_path={{ pgsql_trans_fw_schema }},public;
      \i {{ casfri_dir }}/helperfunctions/geohistory/geohistory.sql
      EOF

  become: yes
  become_user: "{{ postgresql_service_user }}"
